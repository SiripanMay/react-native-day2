/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View,Modal,TouchableOpacity ,Button,Alert} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {

   
        state = {
          modalVisible: false,
          message:''
        };
      
        onShowModal(value) {
          this.setState({modalVisible: true,message:value});
        }
        onHideModal(){
            this.setState({modalVisible: false,message:''});
        }


  render() {
      
    return (
      <View style={styles.container}>
        <Modal
         visible={this.state.modalVisible}
         transparent={true}
         style={styles.modalView}
        >
            <TouchableOpacity
            onPress={() => {this.onHideModal()}}
            style={[styles.modalView,styles.center]}
            >   
            <Text style={styles.textModal}>{this.state.message}</Text>
            </TouchableOpacity>
            
        </Modal>

        <View style={styles.header}>
          <Text style={styles.headerText}>Newss</Text>
        </View>
        <View style={styles.content}>

          <View style={styles.row}>
            <View style={[styles.box1,styles.center]}>
            <TouchableOpacity onPress={(value) => {this.onShowModal(value='Lorem...1')}}>
                <Text >Lorem...1</Text>
            </TouchableOpacity>
            </View>
            <View style={[styles.box2,styles.center]}>
            <TouchableOpacity onPress={(value) => {this.onShowModal(value='Lorem...2')}}>
                <Text >Lorem...2</Text>
            </TouchableOpacity>
            </View>
          </View>

            <View style={styles.row}>
            <View style={[styles.box1,styles.center]}>
            <TouchableOpacity onPress={(value) => {this.onShowModal(value='Lorem...3')}}>
                <Text >Lorem...3</Text>
            </TouchableOpacity>
            </View>
            <View style={[styles.box2,styles.center]}>
            <TouchableOpacity onPress={(value) => {this.onShowModal(value='Lorem...4')}}>
                <Text >Lorem...4</Text>
            </TouchableOpacity>
            </View>
          </View>

          <View style={styles.row}>
            <View style={[styles.box1,styles.center]}>
            <TouchableOpacity onPress={(value) => {this.onShowModal(value='Lorem...5')}}>
                <Text >Lorem...5</Text>
            </TouchableOpacity>
            </View>
            <View style={[styles.box2,styles.center]}>
            <TouchableOpacity onPress={(value) => {this.onShowModal(value='Lorem...6')}}>
                <Text >Lorem...6</Text>
            </TouchableOpacity>
            </View>
          </View>

        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'green',
  },
  header: {
    backgroundColor: 'white',
    alignItems: 'center',
  },
  headerText: {
    color: 'red',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 30,
  },
  textModal: {
    color: 'white',
    fontSize: 50,
    fontWeight: 'bold',
    padding: 30,
  },
  content: {
    backgroundColor: 'blue',
    flex: 1,
    flexDirection: 'column'
  },
  box1: {
    backgroundColor: 'green',
    flex: 1,
    margin: 14,
  },
  box2: {
    backgroundColor: 'purple',
    flex: 1,
    margin: 14,
  },
  row: {
    backgroundColor: 'pink',
    flex: 1,
    flexDirection: 'row'
  },
  bottonBox:{
      flex:1,
      padding:30
  },
  center:{
      justifyContent:'center',
      alignItems:'center'
  },
  modalView:{
      position:'absolute',
      top:20,
      right:20,
      left:20,
      bottom:20,
      backgroundColor:'rgba(0,0,0,0.6)',
      flex:1
  }
});
