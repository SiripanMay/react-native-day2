/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image,TextInput,TouchableOpacity,Button,Alert } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {

  state={
    username:'',
    password:''
  }

  onClickLogin(){
    const Username = this.state.username
    const Password = this.state.password
    if(Username==='admin'&& Password==='eiei'){
      Alert.alert('Yahoo !!!')
    }else{
      Alert.alert('Username or Password wrong :(')
    }
  }

  

  render() {
    return (
      <View style={styles.container} >
        <View style={styles.content}>
          <View style={[styles.box1, styles.center]}>
            <Image source={require('./cat.jpg')} style={[styles.img, styles.center]} />
          </View>

          <View style={[styles.box2, , styles.center]}>
          <Text>{this.state.username}-{this.state.password}</Text>
            <View style={[styles.inputBox1, styles.center]}>
              <View style={styles.textInputBox}>
                <TextInput 
                style={styles.textInput} 
                placeholder='username' 
                onChangeText={value=>{this.setState({username:value})}}/>
              </View>
              <View style={styles.textInputBox}>
              <TextInput 
              style={styles.textInput} 
              placeholder='password' 
              onChangeText={value=>{this.setState({password:value})}}/>
              </View>
            </View>
            <View style={[styles.inputBox1, styles.center]}>
              <View>
              <TouchableOpacity>
                <Button title="Login" onPress={()=>{this.onClickLogin()}} style={styles.buttonBox}/>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
  },
  content: {
    backgroundColor: 'blue',
    flex: 1,
    flexDirection: 'column'
  },
  box1: {
    backgroundColor: 'gray',
    flex: 1,

  },
  box2: {
    backgroundColor: 'gray',
    flex: 1,

  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  img: {
    backgroundColor: 'white',
    borderRadius: 150,
    width: 250,
    height: 250,
  },
  textInput: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 5,

  },
  textInputBox: {
    backgroundColor: 'pink',
    margin: 10,
    alignItems: 'center',
    flexDirection: 'column',
    width: 300,
    height: 60,
  },
  inputBox1: {
    flex: 1
  },
  buttonBox:{
    color: 'pink',
  }

});
